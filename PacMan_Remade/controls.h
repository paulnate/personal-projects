void gamestart()
{
    x = x_pacman;
    y= y_pacman;
    int dir=2,dir_ghost=2;

    sprintf(pointsstr, "POINTS: %d", points);
    ShowLivesPoints(squaresize,pointsstr,livesstr);
    alive=true;

    pthread_t tidPm, tidGh, tidGhC,tidGhO;
    pthread_create(&tidGh, NULL,ghost_thread,NULL);
    pthread_create(&tidGhC, NULL,ghostC_thread,NULL);
    pthread_create(&tidGhO, NULL,ghostO_thread,NULL);
    pthread_create(&tidPm, NULL,pm_thread,NULL);

    pthread_join(tidGh, NULL);
    pthread_join(tidGhC, NULL);
    pthread_join(tidGhO, NULL);
    pthread_join(tidSpeed, NULL);
    pthread_join(tidPm, NULL);

    printf("game over! points: %d",points);
}
