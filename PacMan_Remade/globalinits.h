///MAPC
int mapC[1000][1000][4];    //matrice tridimensionala globala (pentru a fi intializata default cu valoarea 0)

///STRINGS
char pointsstr[1000]="POINTS: 0";   //string in care se memoreaza numarul de puncte (pentru afisare)
char livesstr[100]="LIVES: 3";  //string in care se memoreaza numarul de vieti (pentru afisare)
int lives =3;   //numarul de vieti
int points=0;   //numarul de puncte

int dir;    //variabila care retine directia caracterului PacMan (0-stanga, 1-sus, 2-dreapta, 3-jos)

///COORDINATES
int x_pacman,y_pacman;  //coordonatele initiale ale caracterului PacMan

int x_ghost,y_ghost;    //coordonatele in timp real ale fantomei Rosii
int x_ghost_init,y_ghost_init;     //coordonatele initiale ale fantomei Rosii

int x_ghostC,y_ghostC;  //coordonatele in timp real ale fantomei Cyan
int x_ghostC_init,y_ghostC_init;  //coordonatele initiale ale fantomei Cyan

int x_ghostO,y_ghostO;  //coordonatele in timp real ale fantomei Galbene
int x_ghostO_init,y_ghostO_init;  //coordonatele initiale ale fantomei Galbene

int x_node,y_node;  //coordonatele care retin noduri

int nodes[900][900];

int x, y; //coordonatele in timp real pt PacMan

///SPEED
int speed=5;    //viteza initiala a fantomei Rosii
int pm_speed=0; //viteza initiala PacMan

bool can_eat=false; //bool pentru verificare. Daca un coin mare e mancat, ai posibilitatea de a manca fantoma Rosie (pt aprox 5 secunde)

int colorx=GREEN;     //retine culoarea initiala a fantomei Rosii (cand poate fi mancata devine alba)

bool alive;         //bool folosit pentru verificare. Daca PacMan e in viata = true, daca nu = false

int i=0,j=0,r=50,a[100][100];

int n=30;

int squaresize = SIZE_X/n,raza=squaresize/2-2;

char ch;


