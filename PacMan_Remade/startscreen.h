void gameover()
{
    char ch ;
    settextstyle(6,0,7);
    int width = textwidth("GAME OVER!");
    int height =textheight("GAME OVER!");
    int midx=getmaxx()/2;
    int midy=getmaxy()/2;
    outtextxy(midx-width/2, midy-height/2,"GAME OVER!");
    setcolor(YELLOW);
    settextstyle(8,0,5);
    outtextxy(midx-width/3, midy+height/2,pointsstr);
    while(ch!='x')
    {
        if(points <1000)
        outtextxy(0,SIZE_Y-100," You kinda suck..");
        else if(points > 1000 && points < 5000)
        outtextxy(0,SIZE_Y-100," Congrats!");
        else if(points > 5000)
        outtextxy(0,SIZE_Y-100," That's a huge score! ");
    }
    outtextxy(0,0,"OK fine, you can leave..");
    delay(1000);
}
void startscreen()
{
    bool pressed=false;
    while(!pressed)
    {
        settextstyle(6,0,8);
        int width = textwidth("START!");
        int height =textheight("START!");
        int midx=getmaxx()/2;
        int midy=getmaxy()/2;

        int colorSquare=YELLOW;

        setcolor(colorSquare);
        rectangle(midx-width/2-3, midy-height/2-1,midx+width-190, midy+height+5);
        rectangle(midx-width/2-3-10, midy-height/2-1-10,midx+width-190+10, midy+height+5+10);
        setfillstyle(XHATCH_FILL,colorSquare);
        floodfill(midx-width/2-3-5,midy-height/2-5,colorSquare);

        setcolor(WHITE);
        outtextxy(midx-width/2, midy-height/2,"START!");

        settextstyle(8,0,5);
        outtextxy(midx-width/2+20, midy+height/2,"Are you ready?");

        setcolor(colorSquare);
        rectangle(midx-width/2-15,midy+height/2+90,midx+width/2-220,midy+height+110);
        rectangle(midx-width/2-25,midy+height/2+80,midx+width/2-210,midy+height+120);
        setfillstyle(SLASH_FILL,colorSquare);
        floodfill(midx-width/2-14,midy+height/2+89,colorSquare);
        setcolor(WHITE);
        outtextxy(midx-width/2, midy+height/2+100,"YES: Y");

        setcolor(colorSquare);
        rectangle(midx-width/2-15+270,midy+height/2+90,midx+width/2+20,midy+height+110);
        rectangle(midx-width/2-15+260,midy+height/2+80,midx+width/2+30,midy+height+120);
        setfillstyle(BKSLASH_FILL,colorSquare);
        floodfill(midx-width/2-15+269,midy+height/2+89,colorSquare);
        setcolor(WHITE);        outtextxy(midx-width/2+270, midy+height/2+100,"NO: N");

        settextstyle(8,0,2);
        int controlheight=textheight("CONTROLS:");

        setcolor(colorSquare);
        rectangle(0,0,200,120);
        outtextxy(0, 0,"CONTROLS: ");
        outtextxy(0, controlheight," W - move up");
        outtextxy(0, controlheight*2," A - move left");
        outtextxy(0, controlheight*3," S - move down");
        outtextxy(0, controlheight*4," D - move right");

        setfillstyle(HATCH_FILL,RED);
        floodfill(500,100,colorSquare);

        char ch = getch();
        if(ch=='y')
        {
            cleardevice();
            pressed = true;
        }
        if(ch=='n')
        {
            cleardevice();
            setcolor(WHITE);
            settextstyle(3,0,8);
            outtextxy(midx-width/2, midy-height/2,"TOO BAD! >:)");
            delay(500);
            cleardevice();            pressed = true;
        }
        setcolor(WHITE);
    }
}
void start()
{
    bool start = false;
    //int points =0;

    startscreen();

    print_map();

    while(!start)
    {

    if(kbhit())
        start = true;
    if(start)
    gamestart();
    }

    sprintf(pointsstr,"Points: %d", points);

    ///game over
    cleardevice();
    gameover();

}
