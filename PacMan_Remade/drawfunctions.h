void draw_PacMan(int x, int y, int raza)
{
    setcolor(colorx);
    circle(x,y,raza-2);
}
int center_square_x(int left, int top, int right, int bot)
{
    int x=(left+right)/2;
    return x;
}
int center_square_y(int left, int top, int right, int bot)
{
    int y=(top+bot)/2;
    return y;
}
void draw_coin(int x, int y, int raza)
{
    setcolor(14);
    circle(x,y,raza/2);

    setcolor(WHITE);
}
void draw_square(int i,int j,int squaresize)
{
    int color = GREEN;

    setcolor(color);
    rectangle((j-1) * squaresize,i * squaresize,(j)*squaresize,(i+1) * squaresize);
    setfillstyle(SOLID_FILL,color);


    x=center_square_x((j-1) * squaresize,i * squaresize,(j)*squaresize,(i+1) * squaresize);
    y=center_square_y((j-1) * squaresize,i * squaresize,(j)*squaresize,(i+1) * squaresize);
    floodfill(x,y,color);
    setcolor(WHITE);

}
void draw_ghost(int x,int y,int color)
{
    int r=squaresize/2;
    //corp fantoma
    setcolor(color);
    circle(x,y-r/2,r/2);

    setcolor(color);
    rectangle(x-r/2,y-r/2,x+r/2,y+r/2);

    //ochi
    setcolor(WHITE);

    circle(x+r/4,y-r/2,r/4);
    circle(x-r/4,y-r/2,r/4);

    setcolor(BLACK);

    circle(x+raza/4,y-r/2,r/6);
    circle(x-raza/4,y-r/2,r/6);

    setcolor(WHITE);
}
void remove_ghost(int x, int y)
{
    int l,t,r,b;
    l=x-squaresize/2;
    t=y-squaresize/2;
    r=x+squaresize/2;
    b=y+squaresize/2;
    setfillstyle(SOLID_FILL,BLACK);
    bar(l+1,t+1,r,b);

    setcolor(WHITE);
}
void Remove(int x,int y,int squaresize)  //x,y center of square
{


    int l,t,r,b;
    l=x-squaresize/2;
    t=y-squaresize/2;
    r=x+squaresize/2;
    b=y+squaresize/2;
    setfillstyle(SOLID_FILL,BLACK);
    bar(l+1,t+1,r,b);
    setcolor(WHITE);
}
