#include <stdio.h>
#include <stdlib.h>
#include <graphics.h>
#include <conio.h>
#include <pthread.h>
#include <math.h>

///SCREEN SIZE
#define SIZE_Y  900
#define SIZE_X  900

#include <queue>

using std::queue;

///PERSONAL HEADERS
#include "globalinits.h"
#include "drawfunctions.h"
#include "initmap.h"
#include "animations.h"
#include "threads.h"
#include "controls.h"
#include "startscreen.h"


using namespace std;


int main()
{
    //deschidere fisier + citire n
    FILE * file;
    file=fopen("MAP.txt","r");
    fscanf(file,"%d",&n);

    //citire matrice din fisier
    read_map(a,n,file);

    //afisare matrice
	initwindow(SIZE_X,SIZE_Y);

	//inceput joc
    start();
    return 0;
}
