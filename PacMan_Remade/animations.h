void move_up_ghost(int &x,int &y,int color, int speed)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x,y-i);
        draw_ghost(x,y-i+squaresize/7,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30-speed);
    }
    mapC[x][y][2]=0;
    y=y-squaresize;
    mapC[x][y][2]=1;
}
void move_down_ghost(int &x,int &y, int color,int speed)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x,y+i);
        draw_ghost(x,y+i+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30-speed);
    }
    mapC[x][y][2]=0;
    y=y+squaresize;
    mapC[x][y][2]=1;
}
void move_left_ghost(int &x,int &y, int color,int speed)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x-i,y);
        draw_ghost(x-i,y+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30-speed);
    }
    mapC[x][y][2]=0;
    x=x-squaresize;
    mapC[x][y][2]=1;
}
void move_right_ghost(int &x,int &y, int color,int speed)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x+i,y);
        draw_ghost(x+i+2,y+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30-speed);
    }
    mapC[x][y][2]=0;
    x=x+squaresize;
    mapC[x][y][2]=1;
}
void move_up_ghostC(int &x,int &y,int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x,y-i);
        draw_ghost(x,y-i+squaresize/7,color);

        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);

        delay(40);
    }
    mapC[x][y][2]=0;
    y=y-squaresize;
    mapC[x][y][2]=2;
}
void move_down_ghostC(int &x,int &y, int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x,y+i);
        draw_ghost(x,y+i+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(40);
    }
    mapC[x][y][2]=0;
    y=y+squaresize;
    mapC[x][y][2]=2;
}
void move_left_ghostC(int &x,int &y, int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x-i,y);
        draw_ghost(x-i,y+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(40);
    }
    mapC[x][y][2]=0;
    x=x-squaresize;
    mapC[x][y][2]=2;
}
void move_right_ghostC(int &x,int &y, int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x+i,y);
        draw_ghost(x+i+2,y+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(40);
    }
    mapC[x][y][2]=0;
    x=x+squaresize;
    mapC[x][y][2]=2;
}
void move_up_ghostO(int &x,int &y,int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x,y-i);
        draw_ghost(x,y-i+squaresize/7,color);

        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);

        delay(30);
    }
    mapC[x][y][2]=0;
    y=y-squaresize;
    mapC[x][y][2]=3;
}
void move_down_ghostO(int &x,int &y, int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x,y+i);
        draw_ghost(x,y+i+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30);
    }
    mapC[x][y][2]=0;
    y=y+squaresize;
    mapC[x][y][2]=3;
}
void move_left_ghostO(int &x,int &y, int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x-i,y);
        draw_ghost(x-i,y+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30);
    }
    mapC[x][y][2]=0;
    x=x-squaresize;
    mapC[x][y][2]=3;
}
void move_right_ghostO(int &x,int &y, int color)
{
    int i;
    for (i=0; i<squaresize; i+=squaresize/8)
    {
        remove_ghost(x,y);
        remove_ghost(x+i,y);
        draw_ghost(x+i+2,y+squaresize/4-2,color);
        if(mapC[x][y][1]==1)
        draw_coin(x,y,squaresize/4+2);
        else if(mapC[x][y][3]==1)
        draw_coin(x,y,squaresize/2);
        delay(30);
    }
    mapC[x][y][2]=0;
    x=x+squaresize;
    mapC[x][y][2]=3;
}

///Alg. Lee implementation
int di[4]={-squaresize,0,squaresize,0};
int dj[4]={0,-squaresize,0,squaresize};

bool isValid(int i, int j)
{
    if(i<0 || j<0 || i>SIZE_X || j>SIZE_X)
        return false;
    if(nodes[i][j]==-1)
        return false;
    return true;
}
int vizitat[900][900];
int vizitatC[900][900];
int vizitatO[900][900];

double euclid_dist(int x,int y, int x_gh, int y_gh)
{
    return sqrt((x-x_gh) * (x-x_gh)+(y-y_gh) * (y-y_gh));
}
void detdir()
{
    int i,j,i_next,j_next;
    i=x;
    j=y;
    std::pair <int, int> aux;
    while(vizitat[i][j]>2)
    {
        for(int dirgh=0;dirgh<4;dirgh++)
        {
            i_next= i+di[dirgh];
            j_next= j+dj[dirgh];
            if(vizitat[i_next][j_next]==vizitat[i][j]-1)
            {
                i=i_next;
                j=j_next;
                break;
            }
        }
    }
    if(x_ghost==i)
    {
        if(y_ghost>j)
            move_up_ghost(x_ghost,y_ghost,RED,speed);
        else if(y_ghost<j)
            move_down_ghost(x_ghost,y_ghost,RED,speed);
    }
    else if(y_ghost==j)
    {
        if(x_ghost>i)
            move_left_ghost(x_ghost,y_ghost,RED,speed);
        else if(x_ghost<i)
            move_right_ghost(x_ghost,y_ghost,RED,speed);
    }

}
void detdirC()
{
    int i,j,i_next,j_next;
    i=x_ghost;
    j=y_ghost;
    std::pair <int, int> aux;
    while(vizitatC[i][j]>2)
    {
        for(int dirgh=0;dirgh<4;dirgh++)
        {
            i_next= i+di[dirgh];
            j_next= j+dj[dirgh];
            if(vizitatC[i_next][j_next]==vizitatC[i][j]-1)
            {
                i=i_next;
                j=j_next;
                break;
            }
        }
    }
    if(x_ghostC==i)
    {
        if(y_ghostC>j)
            move_up_ghostC(x_ghostC,y_ghostC,CYAN);
        else if(y_ghostC<j)
            move_down_ghostC(x_ghostC,y_ghostC,CYAN);
    }
    else if(y_ghostC==j)
    {
        if(x_ghostC>i)
            move_left_ghostC(x_ghostC,y_ghostC,CYAN);
        else if(x_ghostC<i)
            move_right_ghostC(x_ghostC,y_ghostC,CYAN);
    }

}
void detdirO(int x, int y)
{
    int i,j,i_next,j_next;
    i=x;
    j=y;

    std::pair <int, int> aux;
    while(vizitatO[i][j]>2)
    {
        for(int dirgh=0;dirgh<4;dirgh++)
        {
            i_next= i+di[dirgh];
            j_next= j+dj[dirgh];
            if(vizitatO[i_next][j_next]==vizitatO[i][j]-1)
            {
                i=i_next;
                j=j_next;
                break;
            }
        }
    }
    if(x_ghostO==i)
    {
        if(y_ghostO>j)
            move_up_ghostO(x_ghostO,y_ghostO,YELLOW);
        else if(y_ghostO<j)
            move_down_ghostO(x_ghostO,y_ghostO,YELLOW);
    }
    else if(y_ghostO==j)
    {
        if(x_ghostO>i)
            move_left_ghostO(x_ghostO,y_ghostO,YELLOW);
        else if(x_ghostO<i)
            move_right_ghostO(x_ghostO,y_ghostO,YELLOW);
    }

}

void LEE(int x, int y)
{

    memset(vizitat,0,sizeof(vizitat));
    int i,j,i_next,j_next;
    queue < std::pair < int , int > > que;
    que.push(std::make_pair(x_ghost,y_ghost));
    vizitat[x_ghost][y_ghost]=1;

    bool found=false;
    while(!que.empty() && !found)
    {
        i=que.front().first;
        j=que.front().second;
        que.pop();
        for(int dirgh=0;dirgh<4;dirgh++)
        {
            i_next= i+di[dirgh];
            j_next= j+dj[dirgh];
            if(isValid(i_next,j_next) && vizitat[i_next][j_next]<1)
            {
                vizitat[i_next][j_next]=vizitat[i][j]+1;
                que.push(std::make_pair(i_next,j_next));
                if(x==i_next && y==j_next)
                {
                    found=true;
                }
            }
        }
    }
}
void LEEC(int x, int y)
{

    memset(vizitatC,0,sizeof(vizitatC));
    int i,j,i_next,j_next;
    queue < std::pair < int , int > > queC;
    queC.push(std::make_pair(x_ghostC,y_ghostC));
    vizitatC[x_ghostC][y_ghostC]=1;

    bool found=false;
    while(!queC.empty() && !found)
    {
        i=queC.front().first;
        j=queC.front().second;
        queC.pop();
        for(int dirgh=0;dirgh<4;dirgh++)
        {
            i_next= i+di[dirgh];
            j_next= j+dj[dirgh];
            if(isValid(i_next,j_next) && vizitatC[i_next][j_next]<1)
            {
                vizitatC[i_next][j_next]=vizitatC[i][j]+1;
                queC.push(std::make_pair(i_next,j_next));
                if(x==i_next && y==j_next)
                {
                    found=true;
                }
            }
        }
    }
}
void LEEO(int x, int y)
{

    memset(vizitatO,0,sizeof(vizitatO));
    int i,j,i_next,j_next;
    queue < std::pair < int , int > > queO;
    queO.push(std::make_pair(x_ghostO,y_ghostO));
    vizitatO[x_ghostO][y_ghostO]=1;

    bool found=false;
    while(!queO.empty() && !found)
    {
        i=queO.front().first;
        j=queO.front().second;
        queO.pop();
        for(int dirgh=0;dirgh<4;dirgh++)
        {
            i_next= i+di[dirgh];
            j_next= j+dj[dirgh];
            if(isValid(i_next,j_next) && vizitatO[i_next][j_next]<1)
            {
                vizitatO[i_next][j_next]=vizitatO[i][j]+1;
                queO.push(std::make_pair(i_next,j_next));
                if(x==i_next && y==j_next)
                {
                    found=true;
                }
            }
        }
    }
}
