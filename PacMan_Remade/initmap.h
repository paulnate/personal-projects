void read_map(int a[100][100],int n, FILE * file)
{
    int i,j;

    for(i=0; i<=n; i++)
    {
        for(j=0; j<=n; j++)
        {
            fscanf(file,"%c",&a[i][j]);
        }
    }
}
void print_map()
{
    int i,j,x_wall,y_wall,x_coin,y_coin;


    for(i=0; i<=n; i++)
    {
        for(j=0; j<=n; j++)
        {
            if(a[i][j]=='x')
            {

                draw_square(i,j,squaresize);


                x_wall=j*squaresize-squaresize/2;
                y_wall=i*squaresize+squaresize/2;


                mapC[x_wall][y_wall][0]=1;


                nodes[x_wall][y_wall]=-1;
            }
            else
            {

                x_node=j*squaresize-squaresize/2;
                y_node=i*squaresize+squaresize/2;


                nodes[x_node][y_node]=1;

                if (a[i][j]=='s')
                {

                    x_pacman=j*squaresize-squaresize/2;
                    y_pacman=i*squaresize+squaresize/2;


                    draw_PacMan(x_pacman,y_pacman,squaresize/2-2);
                }
                else if (a[i][j]=='o')
                {

                    x_coin=j*squaresize-squaresize/2;
                    y_coin=i*squaresize+squaresize/2;


                    draw_coin(x_coin,y_coin,squaresize/4+2);


                    mapC[x_coin][y_coin][1]=1;
                }
                else if (a[i][j]=='O')
                {

                    x_coin=j*squaresize-squaresize/2;
                    y_coin=i*squaresize+squaresize/2;


                    draw_coin(x_coin,y_coin,squaresize/2);


                    mapC[x_coin][y_coin][3]=1;
                }
                else if(a[i][j]=='f')
                {

                    x_ghost=j*squaresize-squaresize/2;
                    y_ghost=i*squaresize+squaresize/2;


                    x_ghost_init=x_ghost;
                    y_ghost_init=y_ghost;


                    draw_ghost(x_ghost,y_ghost+squaresize/4-2,RED);


                    mapC[x_ghost][y_ghost][2]=1;

                }
                else if(a[i][j]=='v')
                {

                    x_ghostC=j*squaresize-squaresize/2;
                    y_ghostC=i*squaresize+squaresize/2;

                    x_ghostC_init=x_ghostC;
                    y_ghostC_init=y_ghostC;


                    draw_ghost(x_ghostC,y_ghostC+squaresize/4-2,CYAN);


                    mapC[x_ghostC][y_ghostC][2]=2;
                }

                else if(a[i][j]=='h')
                {

                    x_ghostO=j*squaresize-squaresize/2;
                    y_ghostO=i*squaresize+squaresize/2;

                    x_ghostO_init=x_ghostO;
                    y_ghostO_init=y_ghostO;


                    draw_ghost(x_ghostO,y_ghostO+squaresize/4-2,YELLOW);


                    mapC[x_ghostO][y_ghostO][2]=3;
                }
            }
        }
    }
}
