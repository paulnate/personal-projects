void *speed_thread(void *vargp);
void Remove(int x,int y,int squaresize);

void ShowLivesPoints(int squaresize,char points[1000],char lives[100])
{
    setcolor(WHITE);
    int x=SIZE_X-squaresize*7;
    int y=0;
    settextstyle(4,0,1);
    outtextxy(x-squaresize*2,y,points);
    outtextxy(x-squaresize*10,y,lives);
    setcolor(WHITE);
}
void move_up(int &x,int &y)
{
    int i;
    for (i=0; i<squaresize; i+=raza/4)
    {
        Remove(x,y,squaresize);
        Remove(x,y-i,squaresize);
        draw_PacMan(x,y-i-2,raza);
        delay(20-pm_speed);
    }
    y=y-squaresize;
}
void move_down(int &x,int &y)
{
    int i;
    for (i=0; i<squaresize; i+=raza/4)
    {
        Remove(x,y,squaresize);
        Remove(x,y+i,squaresize);
        draw_PacMan(x,y+i+2,raza);
        delay(20-pm_speed);
    }
    y=y+squaresize;
}
void move_left(int &x,int &y)
{
    int i;
    for (i=0; i<squaresize; i+=raza/4)
    {
        Remove(x,y,squaresize);
        Remove(x-i,y,squaresize);
        draw_PacMan(x-i-2,y,raza);
        delay(20-pm_speed);
    }
    x=x-squaresize;
}
void move_right(int &x,int &y)
{
    int i;
    for (i=0; i<squaresize; i+=raza/4)
    {
        Remove(x,y,squaresize);
        Remove(x+i,y,squaresize);
        draw_PacMan(x+i+2,y,raza);
        delay(20-pm_speed);
    }
    x=x+squaresize;
}
void teleportX(int &x,int y)
{
    int posX=x;
    if(posX<0)
    {
        Remove(x+SIZE_X-5,y,squaresize/2);
        draw_PacMan(x+SIZE_X,y,raza);
        x=x+SIZE_X;
    }
    else if(posX>SIZE_X)
    {
        Remove(x-SIZE_X+5,y,squaresize/2);
        draw_PacMan(x-SIZE_X,y,raza);
        x=x-SIZE_X;
    }
}
void teleportY(int x,int &y)
{
    if(y<0)
    {
        draw_PacMan(x,y+SIZE_Y+2,raza);
        Remove(x,y,squaresize/2);
        y=y+SIZE_Y;
    }
    else if(y>SIZE_Y)
    {
        draw_PacMan(x,y-SIZE_Y-2,raza);
        Remove(x,y,squaresize/2);
        y=y-SIZE_Y;
    }
}
void death(int &x, int &y)
{
    Remove(x,y,squaresize);
    remove_ghost(x_ghost,y_ghost);
    draw_PacMan(x_pacman,y_pacman,squaresize/2-2);
    draw_ghost(x_ghost,y_ghost+squaresize/4-2,RED);


    x=x_pacman;
    y=y_pacman;
    lives--;
    sprintf(livesstr, "LIVES: %d", lives);
    ShowLivesPoints(squaresize,pointsstr,livesstr);
}
int collision(int x, int y,int dir)
{
    ///dir: 0-left,1-top,2-right,3-bot

    ///verif daca are coliziuni
    if(mapC[x][y-squaresize][1]==1 && dir==1)
    {

        return 3;
    }
    else if(mapC[x][y+squaresize][1]==1 && dir==3)
    {

        return 3;
    }
    else if(mapC[x+squaresize][y][1]==1 && dir==2)
    {

        return 3;
    }
    else if(mapC[x-squaresize][y][1]==1 && dir==0)
    {

        return 3;
    }



    if(mapC[x][y-squaresize][3]==1 && dir==1)
    {

        return 4;
    }
    else if(mapC[x][y+squaresize][3]==1 && dir==3)
    {

        return 4;
    }
    else if(mapC[x+squaresize][y][3]==1 && dir==2)
    {

        return 4;
    }
    else if(mapC[x-squaresize][y][3]==1 && dir==0)
    {

        return 4;
    }
    if(mapC[x][y-squaresize][0]==1 && dir==1)
        return 1;
    else if(mapC[x][y+squaresize][0]==1 && dir==3)
        return 1;
    else if(mapC[x+squaresize][y][0]==1 && dir==2)
        return 1;
    else if(mapC[x-squaresize][y][0]==1 && dir==0)
        return 1;

    return 0;

    //return 3-coin 0-gol, return 1-wall return 2-ghost
}

pthread_t tidSpeed;

void movedir(int &x, int &y, int &points)
{
    if(((mapC[x][y][2]==1 || mapC[x-squaresize][y][2]==1 ||mapC[x+squaresize][y][2]==1 || mapC[x][y+squaresize][2]==1 || mapC[x][y-squaresize][2]==1) || (mapC[x][y][2]==2 || mapC[x-squaresize][y][2]==2 ||mapC[x+squaresize][y][2]==2 || mapC[x][y+squaresize][2]==2 || mapC[x][y-squaresize][2]==2) || (mapC[x][y][2]==3 || mapC[x-squaresize][y][2]==3 ||mapC[x+squaresize][y][2]==3 || mapC[x][y+squaresize][2]==3 || mapC[x][y-squaresize][2]==3)) && !can_eat)
        death(x,y);
    else if(can_eat && (mapC[x][y][2]==1 || mapC[x-squaresize][y][2]==1 ||mapC[x+squaresize][y][2]==1 || mapC[x][y+squaresize][2]==1 || mapC[x][y-squaresize][2]==1))
    {
        mapC[x_ghost][y_ghost][2]=0;
        remove_ghost(x,y);
        points+=500;
        x_ghost=x_ghost_init;
        y_ghost=y_ghost_init;
        pthread_create(&tidSpeed, NULL,speed_thread,NULL);
    }
    else if(can_eat && (mapC[x][y][2]==2 || mapC[x-squaresize][y][2]==2 ||mapC[x+squaresize][y][2]==2 || mapC[x][y+squaresize][2]==2 || mapC[x][y-squaresize][2]==2))
    {
        mapC[x_ghostC][y_ghostC][2]=0;
        remove_ghost(x,y);
        points+=500;
        x_ghostC=x_ghostC_init;
        y_ghostC=y_ghostC_init;
        pthread_create(&tidSpeed, NULL,speed_thread,NULL);
    }
    else if(can_eat && (mapC[x][y][2]==3 || mapC[x-squaresize][y][2]==3 ||mapC[x+squaresize][y][2]==3 || mapC[x][y+squaresize][2]==3 || mapC[x][y-squaresize][2]==3))
    {
        mapC[x_ghostO][y_ghostO][2]=0;
        remove_ghost(x,y);
        points+=500;
        x_ghostO=x_ghostO_init;
        y_ghostO=y_ghostO_init;
        pthread_create(&tidSpeed, NULL,speed_thread,NULL);
    }
    mapC[x][y][1]=0;
    if(ch=='d')
    {
        dir=2;
        if(collision(x,y,dir)==0)
        {
            move_right(x,y);
        }
        else if((mapC[x+squaresize][y][2]==1 || mapC[x+squaresize][y][2]==2 || mapC[x+squaresize][y][2]==3) && !can_eat)
        {
            death(x,y);
        }
        else if(collision(x,y,dir)==3)
        {
            mapC[x][y][1]=0;
            move_right(x,y);
            points+=10;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
        }
        else if(collision(x,y,dir)==4)
        {
            move_right(x,y);
            mapC[x][y][3]=0;
            mapC[x][y][1]=0;
            points+=50;
            pm_speed=7;
            can_eat=true;
            colorx=WHITE;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
        }
        teleportX(x,y);
    }
    else if (ch=='a')
    {
        dir=0;
        if(collision(x,y,dir)==0)
            move_left(x,y);
        else if((mapC[x-squaresize][y][2]==1 || mapC[x-squaresize][y][2]==2 || mapC[x-squaresize][y][2]==3) && !can_eat)
        {
            death(x,y);
        }
        else if(collision(x,y,dir)==3)
        {
            move_left(x,y);
            points+=10;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
            mapC[x][y][1]=0;
        }
        else if(collision(x,y,dir)==4)
        {
            move_left(x,y);
            mapC[x][y][3]=0;
            mapC[x][y][1]=0;
            points+=50;
            pm_speed=7;
            can_eat=true;
            colorx=WHITE;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
        }
        teleportX(x,y);
    }
    else if (ch=='w')
    {
        dir=1;
        if(collision(x,y,dir)==0)
        {
            move_up(x,y);
        }
        else if((mapC[x][y-squaresize][2]==1 || mapC[x][y-squaresize][2]==2 || mapC[x][y-squaresize][2]==3) && !can_eat)
        {
            death(x,y);
        }
        else if(collision(x,y,dir)==3)
        {
            move_up(x,y);
            points+=10;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
            mapC[x][y][1]=0;
        }
        else if(collision(x,y,dir)==4)
        {
            move_up(x,y);
            mapC[x][y][3]=0;
            mapC[x][y][1]=0;
            points+=50;
            pm_speed=7;
            can_eat=true;
            colorx=WHITE;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
        }
        teleportY(x,y);
    }
    else if (ch=='s')
    {
        dir=3;
        if(collision(x,y,dir)==0)
            move_down(x,y);
        else if((mapC[x][y+squaresize][2]==1 || mapC[x][y+squaresize][2]==2 || mapC[x][y+squaresize][2]==3) && !can_eat)
        {
            death(x,y);
        }
        else if(collision(x,y,dir)==3)
        {
            move_down(x,y);
            points+=10;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
            mapC[x][y][1]=0;
        }
        else if(collision(x,y,dir)==4)
        {
            move_down(x,y);
            mapC[x][y][3]=0;
            mapC[x][y][1]=0;
            points+=50;
            pm_speed=7;
            can_eat=true;
            colorx=WHITE;
            sprintf(pointsstr,"POINTS: %d",points);
            ShowLivesPoints(squaresize,pointsstr,livesstr);
        }
        teleportY(x,y);
    }
}
void *ghost_thread(void *vargp)
{
    delay(4000);
    while(alive)
    {
        if(points>1000)
            speed=12;
        else if(points>2000)
            speed=16;
        else if(points>3500)
            speed=20;
        else if(points>4500)
            speed=25;
        LEE(x,y);
        detdir();
    }
}
void *ghostC_thread(void *vargp)
{
    delay(6000);
    while(alive)
    {
        LEEC(x_ghost,y_ghost);
        detdirC();
    }
}
void *ghostO_thread(void *vargp)
{
    delay(9000);
    bool reached=true;
    int x_run=SIZE_X-squaresize-squaresize/2;
    int y_run=SIZE_Y-squaresize-squaresize/2;
    while(alive)
    {
        if(reached)
        {
            if(euclid_dist(x,y,x_ghostO,y_ghostO)>2*squaresize)
            {
                LEEO(x,y);
                detdirO(x,y);
            }
            else reached=false;
        }
        else
        {

            LEEO(x_run,y_run);
            detdirO(x_run,y_run);
            if(x_ghostO == x_run && y_ghostO == y_run)
                reached=true;
        }
    }
}
void *speed_thread(void *vargp)
{
        delay(280);
        pm_speed=0;
        can_eat=false;
        colorx=GREEN;
}
void *pm_thread(void *vargp)
{
    while(alive)
    {
        if(kbhit())
        {
            ch = getch();
        }

        movedir(x,y,points);


        if(lives<=0)
        {
            alive=false;
        }
    }
}
